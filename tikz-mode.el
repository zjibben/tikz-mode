(require 'pdf-tools)

(defvar tikz-preamble
  ""
  "TikZ preamble")

(defvar tikz-work-dir "/tmp/tikz"
  "Directory where TikZ builds the preview")

(define-derived-mode tikz-mode plain-tex-mode "TikZ"
  "Major mode for editing TikZ files.

Main command is C-c to rebuild your TikZ file. A preview buffer
will be automatically opened. You can set a preamble in the string
tikz-preamble."
  (setq-local indent-line-function 'latex-indent)
  (define-key (current-local-map) "\C-c\C-c" 'tikz-compile))

(add-to-list 'auto-mode-alist '(".tikz$" . tikz-mode))

(defun tikz-compile ()
  "(Re)Compile your TikZ file."
  (interactive)

  (save-buffer)
  (let ((fname (file-name-nondirectory (buffer-file-name))))
    (let ((default-directory (concat tikz-work-dir "/" fname))
          (work-file (concat tikz-work-dir "/" fname "/" fname))
          (err-buffer-name "*pdflatex output*"))
      (make-directory default-directory t)
      (shell-command (concat "echo '\\documentclass[tikz]{standalone}' > " work-file " && "
                             "echo '" tikz-preamble "' >> " work-file " && "
                             "echo '\\begin{document}' >> " work-file " && "
                             "cat " (buffer-file-name) " >> " work-file " && "
                             "echo '\\end{document}' >> " work-file))

      ;; run pdflatex, switch to an error message buffer if it fails
      (with-temp-buffer
        (if (not (eq (call-process "pdflatex" nil t nil fname) 0))
            (progn
              (get-buffer-create err-buffer-name)
              (let ((err-text (buffer-string)))
                (pop-to-buffer err-buffer-name)
                (erase-buffer)
                (insert err-text)))))

      ;; open buffer of pdf output and fit it to the screen
      (let ((pdf-buffer
             (find-file-noselect (concat (file-name-sans-extension work-file) ".pdf") t)))
        (display-buffer pdf-buffer)
        (set-buffer pdf-buffer)
        (pdf-view-fit-page-to-window)
        (auto-revert-mode)))))
